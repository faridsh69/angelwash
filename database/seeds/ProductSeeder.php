<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product = 
            ['name' => 'محصول', 'user_id' => '2', 'description' => 'توضیحات محصول', 'rang' => 'سفید', 'jens' => 'گیاهی', 'sazande' => 'پک کالا', 'size' => '۲۰ در ۲۰', 'garanti' => 'پک کالا', 'bastebandi' => 'کارتنی', 'toharbastebandi' => '30','price' => '40000','mojodi' => '30', 'vazn' => '1.3',  'code' => '30100'];
        foreach(\App\Models\Type::where('type_id' , '!=', '')->get() as $type)
        {
            var_dump($type->id);
            for($i = 1; $i < 12 ; $i ++){
                $product['name'] = 'ظرف یکبار مصرف'.$i;
                $product['type_id'] = $type->id;
                $product['image_id'] = rand(2,17);
                \App\Models\Product::updateOrCreate($product);
            }
        }
    }
}
