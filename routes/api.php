<?php

Route::get('type/{type_id}', 'ApiController@getType');
Route::get('city/{city_id}', 'ApiController@getCity');
Route::get('order/{order_id}', 'ApiController@getOrder');
Route::post('order/{order_id}', 'ApiController@postOrder');
Route::get('address/{user_id}', 'ApiController@getAddress');
Route::post('address/{order_id}', 'ApiController@postAddress');
Route::post('put-address/{user_id}', 'ApiController@postPutAddress');
Route::group(['prefix' => 'admin'], function () {
	Route::get('comparison-cinema', 'DataController@getComparisonCinema');
	Route::get('comparison-theater', 'DataController@getComparisonTheater');
	Route::get('comparison-daily', 'DataController@getComparisonDaily');
	Route::get('comparison-total', 'DataController@getComparisonTotal');
	Route::get('sale-daily/{key}', 'DataController@getSaleDaily');
	Route::get('sale-total/{key}', 'DataController@getSaleTotal');
});
// Route::post('voice', 'MasterController@postVoice');