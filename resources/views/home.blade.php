@extends('layout.master')
@section('fluid-container')
<div style="position: absolute;z-index: 1;max-width: 80%">
    <div id="large-header">
        <canvas id="demo-canvas"></canvas>
    </div>
</div>

<div class="row text-center" style="background-image: url('/public/img/a33.jpg');
background-position: cover">
    <div class="col-xs-10 col-xs-offset-1 ">            

        <h1 class="bold  hug-size" style="color: white" >
            کافه کارواش لوکس فرشته
        </h1>
        <h3 style="color: white">  
            Customise your car with us
        </h3>
        </div>
    <div class="col-xs-offset-0 col-xs-11">
        <div class="">
            @if(!$mobile)
            <div class="ip-slideshow-wrapper">
                <nav>
                    <span class="ip-nav-left"></span>
                    <span class="ip-nav-right"></span>
                </nav>
                <div class="ip-slideshow"></div>
            </div>
            @endif
        </div>        
    </div>
</div>




<div class="row text-center" style="background-color: white">
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-xs-12 ">
        <h2 class="bold">
            <!-- <span class="glyphicon glyphicon-copy"></span> -->
            مزایای رقابتی ما
        </h2>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-sm-4">
        <img src="/public/img/why-3.png" width="70px" alt="مرحله ۱" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            زمان تحویل سریع
        <div class="seperate"></div>
        <small>ما اتومبیل شما را در کوتاهترین زمان به شما تقدیم می نماییم. برای شما سالن انتظاری به همراه کافه و لیون تدارک دده شده.</small>
        </h4>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-2.png" width="70px" alt="مرحله ۲" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            جلب رضایت مشتریان
        <div class="half-seperate"></div>
        <small>پس از یکبار استفاده از خدمات ما مطمن باشید دیگر کارواش دیگری نمی روید!! تمیزی خودرویتان را از ما بخواهید</small>
        </h4>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-1.png" width="60px" alt="مرحله ۳" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            گارانتی خدمات 
            <div class="half-seperate"></div>
        <small>شستشوی بینقص خودروی شما وظیفه ماست.</small>
        </h4>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>









<div class="row">
    <div class="col-xs-12 text-center">
        <h2 id="brands">برندها</h2>

    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <ul class="ch-grid">
            <li>
                <div class="ch-item">   
                    <div class="ch-info mentor1">
                        <h3>Mafra</h3>
                        <p>Registered in Italy<a href="#">Details</a></p>
                    </div>
                    <div class="ch-thumb ch-img-1"></div>
                </div>
            </li>
            <li>
                <div class="ch-item">
                    <div class="ch-info mentor2">
                        <h3>Ecoline wash</h3>
                        <p>Registered in Germany  <a href="#">Details</a></p>
                    </div>
                    <div class="ch-thumb ch-img-2"></div>
                </div>
            </li>
            <li>
                <div class="ch-item">
                    <div class="ch-info mentor3">
                        <h3>Quick clean</h3>
                        <p>Registered in America <a href="#">Details</a></p>
                    </div>
                    <div class="ch-thumb ch-img-3"></div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>

<div class="row text-center" style="background-color: white">
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="main">
        <div class="fs-slider" id="fs-slider">
            <figure>
                <img src="/public/img/a51.jpg" alt="image01" />
                <figcaption>
                    <h3>کارواش</h3>
                    <p>کارواش لوکس فرشته</p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a52.jpg" alt="image02" />
                <figcaption>
                    <p>کارواش لوکس فرشته</p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a53.jpg" alt="image03" />
                <figcaption>
                    <p>کارواش لوکس فرشته</p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a54.jpg" alt="image04" />
                <figcaption>
                    <p>کارواش لوکس فرشته</p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a55.jpg" alt="image05" />
                <figcaption>
                    <p>کارواش لوکس فرشته</p>
                </figcaption>
            </figure>
        </div>
    </div>
</div>


<div class="row" style="background-color: white">
    <div class="col-xs-12 text-center">
        <div class="seperate"></div>
        <div class="seperate"></div>
        <h3>
            ثبت‌نام سریع
        </h3>
    </div>
</div>
<div class="row" style="background-color: white">
    <div class="col-xs-12 ltr">
        <form id="theForm" class="simform text-right" autocomplete="off">
            <div class="simform-inner">
                <ol class="questions">
                    <li>
                        <span><label for="q1">نام خود را وارد کنید؟</label></span>
                        <input id="q1" name="q1" type="text"/>
                    </li>
                    <li>
                        <span><label for="q2">نام خانوادگی؟</label></span>
                        <input id="q2" name="q2" type="text"/>
                    </li>
                    <li>
                        <span><label for="q3">ایمیلت ؟</label></span>
                        <input id="q3" name="q3" type="text"/>
                    </li>
                    <li>
                        <span><label for="q4">تلفنت ؟‌شاید لازم شد زنگت بزنیم</label></span>
                        <input id="q4" name="q4" type="text"/>
                    </li>
                    <li>
                        <span><label for="q5">روش آشناییت با ما</label></span>
                        <input id="q5" name="q5" type="text"/>
                    </li>
                    <li>
                        <span><label for="q6">رمز عبور؟</label></span>
                        <input id="q6" name="q6" type="text"/>
                    </li>
                </ol>
                <button class="submit" type="submit">ارسال جواب‌ها</button>
                <div class="controls">
                    <button class="next"></button>
                    <div class="progress"></div>
                    <span class="number">
                        <span class="number-current"></span>
                        <span class="number-total"></span>
                    </span>
                    <span class="error-message"></span>
                </div>
            </div>
            <span class="final-message"></span>
        </form>         
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>


@endsection
@push('script')
 <script type="text/javascript" src="/public/js/jquery.imgslider.js"></script>
<script type="text/javascript">
    $(function() {

        $( '#fs-slider' ).imgslider();

    });
</script>
<script src="/public/js/classie2.js"></script>
<script src="/public/js/stepsForm.js"></script>
<script>
    var theForm = document.getElementById( 'theForm' );

    new stepsForm( theForm, {
        onSubmit : function( form ) {
            // hide form
            classie.addClass( theForm.querySelector( '.simform-inner' ), 'hide' );

            /*
            form.submit()
            or
            AJAX request (maybe show loading indicator while we don't have an answer..)
            */

            // let's just simulate something...
            var messageEl = theForm.querySelector( '.final-message' );
            messageEl.innerHTML = 'ممنون از پر کردن فرم ما ،به زودی می‌بینیمت';
            classie.addClass( messageEl, 'show' );
        }
    } );
</script>
<script src="/public/js/particlesSlideshow.js"></script>
<script src="/public/js/demo-2.js"></script>
@endpush

