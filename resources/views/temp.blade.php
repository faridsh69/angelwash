<!-- <div class="row" style="display: none;">
    <div class="col-xs-12 text-center">
        <h3>
            ثبت‌نام سریع
        </h3>
    </div>
</div>
<div class="row" style="display: none;">
    <div class="col-xs-12 ltr">
        <form id="theForm" class="simform text-right" autocomplete="off">
            <div class="simform-inner">
                <ol class="questions">
                    <li>
                        <span><label for="q1">نام خود را وارد کنید؟</label></span>
                        <input id="q1" name="q1" type="text"/>
                    </li>
                    <li>
                        <span><label for="q2">نام خانوادگی؟</label></span>
                        <input id="q2" name="q2" type="text"/>
                    </li>
                    <li>
                        <span><label for="q3">ایمیلت ؟</label></span>
                        <input id="q3" name="q3" type="text"/>
                    </li>
                    <li>
                        <span><label for="q4">تلفنت ؟‌شاید لازم شد زنگت بزنیم</label></span>
                        <input id="q4" name="q4" type="text"/>
                    </li>
                    <li>
                        <span><label for="q5">روش آشناییت با ما</label></span>
                        <input id="q5" name="q5" type="text"/>
                    </li>
                    <li>
                        <span><label for="q6">رمز عبور؟</label></span>
                        <input id="q6" name="q6" type="text"/>
                    </li>
                </ol>
                <button class="submit" type="submit">ارسال جواب‌ها</button>
                <div class="controls">
                    <button class="next"></button>
                    <div class="progress"></div>
                    <span class="number">
                        <span class="number-current"></span>
                        <span class="number-total"></span>
                    </span>
                    <span class="error-message"></span>
                </div>
            </div>
            <span class="final-message"></span>
        </form>         
    </div>
</div> -->
<div class="row">
    <div class="col-xs-12 text-center">
    <h3>مربیان</h3>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <ul class="ch-grid">
            <li>
                <div class="ch-item">   
                    <div class="ch-info mentor1">
                        <h3>مهندس شکوری</h3>
                        <p>مربی بخش ... <a href="http://drbl.in/eGjw">رزومه</a></p>
                    </div>
                    <div class="ch-thumb ch-img-1"></div>
                </div>
            </li>
            <li>
                <div class="ch-item">
                    <div class="ch-info mentor2">
                        <h3>حمیدرضا صدیقیان‌راد</h3>
                        <p>مربی بخش ...  <a href="http://drbl.in/eFWR">View on Dribbble</a></p>
                    </div>
                    <div class="ch-thumb ch-img-2"></div>
                </div>
            </li>
            <li>
                <div class="ch-item">
                    <div class="ch-info mentor3">
                        <h3>مهندس اکبر رنجبری</h3>
                        <p>مربی بخش ... <a href="http://drbl.in/eFED">رزومه</a></p>
                    </div>
                    <div class="ch-thumb ch-img-3"></div>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="seperate"></div>
<div class="row">
    <div class="col-xs-12 text-center">
    <a href="/mentor" class="btn btn-lg btn-info big-size"> ادامه مطلب</a>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>

<div class="row text-center" style="background-color: white">
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-xs-12 ">
        <h2 class="bold">
            <!-- <span class="glyphicon glyphicon-copy"></span> -->
            راه‌های ورود به مرکز کارآفرینی طرفه نگار
        </h2>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-sm-4">
        <img src="/public/img/why-3.png" width="70px" alt="مرحله ۱" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            شتابدهی
        <div class="seperate"></div>
        <small>این راه ورود مناسب افرادی است که تنها ایده دارند و برای رسیدن به محصول نیاز به کمک زیادی از ما دارند.</small>
        </h4>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-2.png" width="70px" alt="مرحله ۲" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            سرمایه‌گذاری
        <div class="half-seperate"></div>
        <small>این پلن برای کسانی است که محصول اولیه یا نهایی دارند و برای رشد آن نیاز به سرمایه گذار دارند</small>
        </h4>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-1.png" width="60px" alt="مرحله ۳" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            همکاری
            <div class="half-seperate"></div>
        <small>افرادی که محصول و کاربر دارند و برای رشد قسمتی از آن نیاز به همکاری با طرفه‌نگار دارند.</small>
        </h4>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="main">
        <div class="fs-slider" id="fs-slider">
            <figure>
                <img src="/public/img/a41.png" alt="image01" />
                <figcaption>
                    <h3>Eloquence</h3>
                    <p>American apparel flexitarian put a bird on it, mixtape typewriter irony aesthetic. </p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a42.png" alt="image02" />
                <figcaption>
                    <h3>Quintessential</h3>
                    <p>Cardigan craft beer mixtape, skateboard forage fixie truffaut messenger bag. </p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a43.png" alt="image03" />
                <figcaption>
                    <h3>Surreptitious</h3>
                    <p>Truffaut iphone flexitarian, banh mi thundercats cliche wolf biodiesel gastropub. </p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a44.png" alt="image04" />
                <figcaption>
                    <h3>Imbroglio</h3>
                    <p>Scenester fixie pickled, shoreditch fugiat enim craft beer retro mustache.</p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a45.png" alt="image05" />
                <figcaption>
                    <h3>Lissome</h3>
                    <p>Aute sunt aliqua do sapiente. Food truck ut sustainable thundercats four loko.</p>
                </figcaption>
            </figure>
        </div>
    </div>
</div>

<div class="row" style="background-color: white;padding: 20px;">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                  <h3>
                    درکنار هم می‌توانیم 
                    <img width="90" alt="why" src="/public/img/why.png" class=""> 
                </h3>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-md-offset-0 text-center">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img alt="مرحله ۱" width="57" src="/public/img/s1.png" class="">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                مشاوره برای رسیدن به اهداف
                </p>
                <small>
                    در هر شرایطی ما پشت شما هستیم و به همفکری و مشورت با ما می‌توانید تکیه کنید
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img alt="مرحله ۲" width="60" src="/public/img/s2.png" class="">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                محیط کار صمیمی و فعال
                </p>
                <small>
                    محل کار مهم‌ترین بخش انتخاب ک شغل است مخصوصا زمانی که نیاز به محلی آرام و پویا دارید
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>

            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s3.png" alt="مرحله ۳" width="57">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                ارتباطات موثر
                </p>
                <small>در این مرکز می‌‌توانید با افرادی که استارتاپ موفق داشته اند روبرو شوید و از آنها الگوبرداری کنید</small>
                <div class="seperate"></div>
                <div class="seperate"></div>
                <div class="seperate"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s4.png" alt="مرحله ۱">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                سرمایه گذاری هدفمند
                </p>
                <small>
                    در این محیط برخلاف سایر مراکز کارآفرینی رسیدن شما به محصول نهاییتان هدف قطعی ماست
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>
                <div class="seperate"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s5.png" alt="مرحله ۲">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                مربیان برجسته در کنار شما
                </p>
                <small>
                    در هر شرایطی ما پشت شما هستیم و به همفکری و مشورت با ما می‌توانید تکیه کنید
                </small>
                <div class="seperate"></div>
                <div class="seperate"></div>
                
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="/public/img/s6.png" alt="مرحله ۳">
                <div class="half-seperate"></div>
                <p class="bold big-size">
                آموزش رشد استارتاپ
                </p>
                <small>
                    در هر شرایطی ما پشت شما هستیم و به همفکری و مشورت با ما می‌توانید تکیه کنید
                </small>
            </div>
        </div>
        <div class="seperate"></div>
        <div class="seperate"></div>
    </div>
</div>


<div class="row" style="background: url('img/wall.jpg');padding: 20px;" >
    <div class="seperate" id="introduce"></div>
    <div class="seperate" ></div>
    <div class="seperate" ></div>
    <div class="col-sm-4">
        <h3 class="text-center bold">
            روند کاری
        </h3>
        <h5 style="line-height: 180%">
            همه چیز از یک ایده عالی شروع می‌شود.
            اگر به ایده خود ایمان دارید آن را در این سامانه مطرح کنید. 
            پس از بررسی طرح شما جلساتی مهیا می‌شود که در آنها هرگونه ابهامی برای شما رفع شود.

            <br>
            پس از آن محلی برای تولید و اجرای ایده شما در اختیارتان قرار می‌گیرد که در آن به ساخت محصول می‌پردازید.
            در حین کار به شما راهنمایی هایی ارایه می‌گردد که به بهبود محصول شما کمک می‌کند.
            با کامل شدن محصولتان ارایه ای از آن محصول را برای اعضای هیت مدیره انجام میدهید.
        </h5>
        <h4>
            ساعت کاری مرکز:
            همه روزه از ساعت ۶ صبح الی ۱۰ شب
            <br>
            <br>
            روزهای کاری :
            همه روزه
            <br>
            <br>
            <small>
            آدرس محل کار:
            خیابان میرداماد - کوچه زرنگار - ساختمان هلو
            </small>
             
        </h5>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-8">
        <div class="seperate"></div>
        <img src="/public/img/process.png" class="img-responsive">

        <!-- <img alt="سنگ ساختمانی" src="/public/img/n12.jpg" class=" img-responsive"> -->
    </div>
    <div class="seperate"></div>
</div>

<div class="row" style="background: white;padding: 20px;" >
    <div class="seperate" id="news"></div>
    <div class="seperate" ></div>
    <div class="seperate" ></div>
    <div class="col-sm-12">
        <h3 class="text-center bold">
            استارتاپ‌ها
            <br>
            جای شما خالی است - شما هم می توانید کی از ما باشید 
            <br>
            <br>
            <a href="/register-idea" class="btn btn-primary btn-lg">ثبت استارتاپ</a>
        </h3>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="stage">
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                تیمچه سرا
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">داوودی - شهیدی</span>                             
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.
                                    این سامانه به دنباله خدمت رسانی طرفه نگار به مشتریان هلو ارایه می‌گردد.
                                </p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                آرایشگاه آنلاین
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                آموزش زبان 
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                ورزشگاه آنلاین
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="seperate"></div>
</div>


<div class="row" style="background: white;padding: 20px;" >
    <div class="seperate" id="news"></div>
    <div class="seperate" ></div>
    <div class="seperate" ></div>
    <div class="col-sm-12">
        <h3 class="text-center bold">
            سرمایه گذاری‌ها 
            <br>
            جای شما خالی است - شما هم می توانید کی از ما باشید 
            <br>
            <br>
            <a href="/register-idea" class="btn btn-primary btn-lg">ثبت استارتاپ</a>
        </h3>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="stage">
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                تیمچه سرا
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">داوودی - شهیدی</span>                             
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.
                                    این سامانه به دنباله خدمت رسانی طرفه نگار به مشتریان هلو ارایه می‌گردد.
                                </p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                آرایشگاه آنلاین
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                آموزش زبان 
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
                <li class="scene">
                    <a href="/startup/">
                        <div class="movie" onclick="return true">
                            <div class="poster">
                            <h4 style="color: black;padding: 10px;text-align: center;">
                                ورزشگاه آنلاین
                            </h4>
                            </div>
                            <div class="info">
                                <header>
                                    <h1>تیمچه سرا </h1>
                                    <span class="year">متولد: ۱۳۹۷ </span>
                                    <span class="">طرفه نگار</span>
                                        <span class="rating">  </span>
                                </header>
                                <p>فروشگاه سازی کامل برای تمام کسانی که خواستار فروش آنلاین هستند.</p> 
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="seperate"></div>
</div>