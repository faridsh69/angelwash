<!DOCTYPE html>
<html lang="{{ Lang::locale() }}" dir="{{ Lang::locale() == 'fa' ? 'rtl' : 'ltr' }}">
	<head>
		@include('common.header')
	</head>
	<body>
		<div id="body_id">
			<div class="container-fluid">
				@yield('container')
			</div>
		</div>
		<div class="seperate"></div>
	</body>
</html>