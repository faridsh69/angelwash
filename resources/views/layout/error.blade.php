<!DOCTYPE html>
<html lang="fa" dir="rtl">
	<head>
		<title>لینک نامعتبر</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="سفارش غذا,پیک فود,پیکفود,سفارش آنلاین غذا,خرید اینترنتی غذا,غذای شمالی">
<meta name="author" content="farid shahidi">
<meta name="viewport" content="width=device-width, initial-scale=1,user-scalable=yes">
<meta name="Description" content="توسط این سامانه از تمام رستوران های باکیفیت شمال کشور می توانید سفارش غذا دهید.">
<meta name="google" content="notranslate"/>

<meta name="enamad" content="202790352"/>

<meta property="og:url" content="http://www.paykfood.com/">
<meta property="og:title" content="سفارش آنلاین غذا از رستوران‌های شمال کشور">
<meta property="og:description" content="از تمام رستوران های باکیفیت شمال کشور می توانید سفارش غذا دهید.">
<meta property="og:type" content="website">
<meta property="og:locale" content="fa_IR" />
<meta property="og:locale:alternate" content="ar_IR" />
<meta property="og:image" content="http://www.paykfood.com/public/img/logo.png">
<meta property="og:site_name" content="http://www.paykfood.com/">

<meta property="twitter:card" content="summary">
<meta property="twitter:site" content="http://www.paykfood.com/">
<meta property="twitter:title" content="سفارش آنلاین غذا از رستوران‌های شمال کشور">
<meta property="twitter:description" content="از تمام رستوران های باکیفیت شمال کشور می توانید سفارش غذا دهید.">
<meta property="twitter:creator" content="farid shahidi">
<meta property="twitter:image:src" content="http://www.paykfood.com/public/img/logo.png">
<meta property="twitter:domain" content="http://www.paykfood.com/">

<link rel="canonical" href="http://www.paykfood.com/">
<link rel='icon' href='/public/img/favicon.png' type='image/png'>

<link rel="stylesheet" href="/public/css/bootstrap.min.css">
<link rel="stylesheet" href="/public/css/app.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
	</head>
	<body>
		<div class="double-seperate"></div>
		<div class="double-seperate"></div>
		<div class="double-seperate"></div>
		<div class="container-fluid background-container-fluid" style="background: white">
			@yield('fluid-container')
		</div>
	</body>
</html>