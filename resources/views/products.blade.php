@extends('layout.master')
@section('container')
<div class="row">
	<div class="col-xs-12">
		<h4 class="page-header">
			محصولات ما
		</h4>
	</div>
	@if($data = [ 
		['name' => 'پولیش', 'dsc' => 'پولیش واکس براق کننده بدنه مفرا CARLUX با ظاهری متفاوت برای محافظت از رنگ بدنه خودرو
'],
		['name' => 'خوشبو کننده افشانه مفرا', 'dsc' => 'خوشبو کننده افشانه مفرا با کیفیتی ایده آل و داشتن 6 رایحه گیاهی می تواند بهترین انتخاب برای اتومبیل شما باشد .
'],
		['name' => 'پولیش زبر بدنه', 'dsc' => 'پولیش زبر بدنه'],
		['name' => 'خوشبو کننده', 'dsc' => 'خوشبو کننده Hippy با 6 رایحه متنوع قابل نصب روی دریچه کولر خودرو. 
'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		['name' => 'نام محصول', 'dsc' => 'توضیح محصول'],
		])
	@endif	
	@for($i = 1; $i < 14; $i++)
	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="product-block">
			<div class="product-card" style="min-height: 320px !important">
				<div class="product-image-container text-center">
					<img src="/public/img/a/{{$i}}.jpg" class="product-image" alt="کارواش {{$i}} ">
				</div>
				<div class="bold big-size text-center" style="color:#3361ff">
					<span class="">{{ $data[$i-1]['name'] }}</span>
					<div class="half-seperate"></div>
					<div style="color: #999">
						<span>{{ $data[$i-1]['dsc'] }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endfor
</div>
@endsection