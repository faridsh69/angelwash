<nav class="navbar background-nav " 
{{ Request::segment(1) != 'admin' ? '' : 'data-spy=affix data-offset-top="0"' }}>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="glyphicon glyphicon-menu-hamburger"></span>                           
            </button>
            <a class="navbar-brand {{Request::segment(1) == '' ? 'selected':''}}" href="/" style="display: inline-block;"> 
                <img src="/public/img/logo.png" alt="logo" class="logo-icon" style="width: 60px;display: inline-block;">
                <span class="bold">
                کافه کارواش لوکس فرشته
                </span>
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                
                <li><a href="#brands" class="draw2">
                برندها </a>
                </li>
                <li><a href="/products" class="draw2">
                محصولات</a>
                </li>
                <li><a href="#service" class="draw2">
                خدمات</a>
                </li>
                <li><a href="javascript:void(0)" data-toggle="modal" data-target="#contact-us-modal">تماس با ما</a></li>
                
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <div class="half-seperate"></div>
                <div class="one-third-seperate"></div>
            @if(empty(Auth::user()))
                <div class="half-seperate"></div>
                <a href="/user/register" class="google-a"><span class="glyphicon glyphicon-user"></span> ثبت نام</a>
                |
                <a href="/user/login" class="google-a"><span class="glyphicon glyphicon-log-in"></span> ورود</a>
            @else
                <a href="/admin/profile">
                <span class="glyphicon glyphicon-cog big-size"></span>
                {{ Auth::user() ? Auth::user()->first_name : ''}} 
                {{ Auth::user() ? Auth::user()->last_name : ''}}
                <label class="label label-default" style="margin:3px;padding: 0px 7px 0px 7px">
                اعتبار: {{ \Nopaad\Persian::correct(number_format( Auth::user()->credit , 0, '',',')) }} تومان
                </label>
                @if(\Auth::user()->roles()->count() > 0)
                <br>
                    @foreach(\Auth::user()->roles()->pluck('name') as $role)
                        <small style="position: relative;">
                            <span class="label {{ in_array($role ,['manager']) ? 'label-danger' : 'label-success' }}">{{ trans('roles.' . $role) }}</span>
                        </small>
                    @endforeach
                @endif
                </a>
            @endif
            </ul>
        </div>
    </div>
</nav>
@if(Request::segment(1) != 'admin')
<img src="/public/img/shadow-3.png" width="100%" class="background-nav-shadow">
@else
<div class="affix-fixer"></div>
@endif


<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">تماس با کافه کارواش لوکس فرشته</h4>
            </div>
            <div class="modal-body">
                <h3 class="text-center bold">
                    کافه کارواش لوکس فرشته
                </h3>
                <h5 style="line-height: 180%">
                    تلفن دفتر: ۰۲۱۲۶۸۵۳۹۱۶
                </h5>
                <h5 style="line-height: 180%">
                    همراه: ۰۹۱۹۷۳۵۳۰۱۵
                </h5>
                <div class="half-seperate"></div>
                <h5>
                    ساعت کاری:
                    همه روزه از ساعت ۶ صبح الی ۱۰ شب
                </h5>
                <div class="half-seperate"></div>
                <h5>
                    روزهای کاری :
                    همه روزه
                </h5>
                <div class="half-seperate"></div>
                <h5>
                    آدرس محل کار:
                    خیابان فرشته ( فیاضی ) - مجتمع تجاری داریوش - کافه کارواش لوکس فرشته 
                </h5>
                <div class="half-seperate"></div>
            </div>
        </div>
    </div>
</div>
