@if(Request::segment(1) != 'admin')
<div class="background-footer">
<div class="container">
    <div class="seperate"></div>
    <div class="row">
        <div class="col-sm-4 col-md-3 col-lg-3">
            <h4 class="page-header">
                کافه کارواش لوکس فرشته
            </h4>
        </div>
        <div class="col-xs-11 col-xs-offset-1 col-sm-offset-0 col-md-8 col-lg-9">
            <!-- <img src="/public/img/footer-map.png" class="img-responsive"> -->
            <!-- <div class="seperate"></div> -->
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-4 col-md-3">
            <ul>
                <li>
                <a href="https://www.twitter.com/tecenter" >
                    برندها
                </a></li><li>
                <a href="#" >
                    گالری تصاویر
                </a></li><li>
                <a href="#" >
                    ثبت نام سریع
                </a></li><li>
                <a href="#" >
                    مزایای رقابتی
                </a></li>
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-sm-4 col-md-3">
            <ul>
                <li>
                <a data-toggle="modal" data-target="#contact-us-modal" href="#" >
                    تماس با ما
                </a></li><li>
                <a href="#" >
                    محصولات
                </a></li><li>
                <a href="#" >
                    خدمات
                </a></li><li>
                <a href="#" >
                    قیمت‌ها
                </a></li>
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-sm-4 col-md-3">
           <!--  <ul class="list-unstyled">
                <li>                
                    <a href="/">لینک ۱</a>
                </li>
                <li>                
                    <a href="/">لینک ۱</a>
                </li>
                <li>                
                    <a href="/">لینک ۱</a>
                </li>
                <li>                
                    <a href="/">لینک ۱</a>
                </li>
            </ul> -->
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-md-12 col-md-3 text-right">
            <div>کافه کارواش لوکس فرشته در شبکه های اجتماعی</div>
            <div class="half-seperate"></div>
            <div class="social">
                <a href="https://www.facebook.com/lux-carwash">  </a>
                <a href="https://telegram.me/lux-carwash">  </a>
                <a href="https://www.instagram.com/lux-carwash">  </a>
                <a href="https://www.twitter.com/lux-carwash">  </a>
                <a href="https://www.plus.google.com/lux-carwash">  </a>
                <a href="https://www.linkden.com/lux-carwash">  </a>
            </div>
            <div class="one-third-seperate"></div>
            <p>کلیه حقوق این سایت متعلق به کافه کارواش لوکس فرشته
            می‌باشد.</p>
            <p style="color: #444;font-size: 95%">
                نویسنده سایت :<a href="http://rotbeyek.ir/cv"> مهندس فرید شهیدی</a>
            </p>
        </div>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>
</div>
@endif