<title>کافه کارواش ایتالیایی فرشته</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="کارواش ایتالایی فرشته,کارواش فرشته,کارواش ایتالیایی,کافه کارواش,کارواش آبی فرشته,کارواش الهیه,کافه کارواش لوکس">
<meta name="author" content="farid shahidi">
<meta name="viewport" content="width=device-width, initial-scale=0.7,user-scalable=no">
@if(Request::segment(1) == 'admin')
<meta name="viewport" content="width=900, initial-scale=0.5,user-scalable=yes">
@endif
<meta name="description" content="انجام کلیه خدمات تخصصی نظافت و نگهداری خودرو با مواد ایتالیایی مفرا واقع در فرشته به همراه کافه سرپوشیده و رو باز">
<meta itemprop="name" content="کافه کارواش ایتالیایی فرشته">
<meta itemprop="description" content="کارواش ایتالایی فرشته,کارواش فرشته,کارواش ایتالیایی,کافه کارواش,کارواش آبی فرشته,کارواش الهیه,کافه کارواش لوکس">
<meta itemprop="image" content="/public/img/logo.png">

<meta property="og:url" content="{{ url('/') }}">
<meta property="og:title" content="کافه کارواش ایتالیایی فرشته">
<meta property="og:description" content="کارواش ایتالایی فرشته,کارواش فرشته,کارواش ایتالیایی,کافه کارواش,کارواش آبی فرشته,کارواش الهیه,کافه کارواش لوکس">
<meta property="og:type" content="website">
<meta property="og:locale" content="fa_IR" />
<meta property="og:locale:alternate" content="ar_IR" />
<meta property="og:image" content="/pulic/img/logo.png">
<meta property="og:site_name" content="{{ url('/') }}">

<meta property="twitter:card" content="summary">
<meta property="twitter:site" content="{{ url('/') }}">
<meta property="twitter:title" content="کافه کارواش ایتالیایی فرشته">
<meta property="twitter:description" content="کارواش ایتالایی فرشته,کارواش فرشته,کارواش ایتالیایی,کافه کارواش,کارواش آبی فرشته,کارواش الهیه,کافه کارواش لوکس">
<meta property="twitter:creator" content="farid shahidi">
<meta property="twitter:image" content="/public/img/logo.png">
<meta property="twitter:domain" content="{{ url('/') }}">

<!-- <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css"> -->

<link rel="canonical" href="{{ url('/') }}">
<link rel='icon' href='/public/img/logo.png' type='image/png'>

<link rel="stylesheet" href="/public/css/bootstrap.min.css">
<script src="/public/js/jquery.min.js"></script>
<script src="/public/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/public/css/app.css">
@if(Lang::locale() == 'fa')
<link rel="stylesheet" href="/public/css/rtl.css">
@endif
<link rel="stylesheet" type="text/css" href="/public/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="/public/css/component2.css" />

<link rel="stylesheet" type="text/css" href="/public/css/style4.css" />
<script type="text/javascript" src="/public/js/modernizr.custom.26887.js"></script> 

<link rel="stylesheet" type="text/css" href="/public/css/common.css" />
<link rel="stylesheet" type="text/css" href="/public/css/style3.css" />


<!-- <script src="/public/js/modernizr.custom2.js"></script> -->
<!-- <link rel="stylesheet" href="/public/css/effect-3dbox.css"> -->

<!-- <link rel="stylesheet" type="text/css" href="/public/css/component3.css" /> -->
<!-- <link rel="stylesheet" type="text/css" href="/public/css/buttons.css" /> -->
<!-- <script src="/public/js/modernizr.custom3.js"></script> -->