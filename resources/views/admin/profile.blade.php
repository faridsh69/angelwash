@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
	@if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" action="" method="POST">
			{{ csrf_field() }}
			<div class="panel panel-default">
			<div class="panel-heading">پروفایل کاربر</div>
            <div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
					<td width="110px;">نام</td>
					<td><input type="text" name="first_name" class="form-control"
					value="{{ old('first_name' , \Auth::user()->first_name) }}"></td>
				</tr>
				<tr>
					<td>نام خانوادگی</td>
					<td><input type="text" name="last_name" class="form-control"
					value="{{ old('last_name' , \Auth::user()->last_name) }}"></td>
				</tr>
				<tr>
					<td>ایمیل</td>
					<td><input type="email" name="email" class="form-control"
					value="{{ old('email' , \Auth::user()->email) }}">
					<div class="help-block">برای اطلاع‌رسانی بیشتر</div></td>
				</tr>
				<tr>
					<td>تلفن همراه</td>
					<td><input type="text" name="phone" class="form-control"
					value="{{ old('phone' , \Auth::user()->phone) }}"></td>
				</tr>
				
				<tr>
					<td colspan="2">
					<button type="submit" class="btn btn-success btn-block"> ذخیره تغییرات </button>
					</td>
				</tr>
			</table>			
        	</div>
			</div>
		</form>
		
	</div>
</div>
<div class="seperate"></div>
@endsection

