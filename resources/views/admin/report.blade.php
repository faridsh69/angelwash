@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
		<div class="panel-heading">گزارشات</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-xs-12">
					<comparison-cinema></comparison-cinema>
				</div>
			</div>
			<!-- <form class="form-inline" method="GET">
			  	<div class="form-group">
			    	<label for="name">نام رستوران:</label>
			   	 	<select class="form-control input-sm" id="name" name="restaurant_name">
			   	 	<option value="">همه  ها</option>
			   	 	@foreach(\App\Models\Product::get() as $restaurant)
			   	 	<option value="{{ $restaurant->id }}">{{ $restaurant->name }}
			   	 	</option>
			   	 	@endforeach
			   	 	</select>
			  	</div>
			  	<button type="submit" class="btn btn-default input-sm">جستجو</button>
			</form> -->
			<div class="row">
				<div class="col-xs-12">
					بازدید از صفحه تمام رستوران ها:
					{{ \App\Models\ProductSeen::get()->count() }}
					بار
				</div>
				<div class="col-xs-12">
					تعداد خرید از تمام رستوران ها بصورت پرداخت در محل:
					{{ \App\Models\Order::where('status','preparewillpay')->get()->count() }}
					بار
					-
					{{ \App\Models\Order::where('status','preparewillpay')->get()->sum('price') }}
					تومان
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
				<h3 class="page-header">فروش امروز {{ \Nopaad\jDate::forge( strtotime('today') )->format(' %Y/%m/%d') }}</h3>
				<!-- <br>
				<br>
				{{ strtotime('today') }}
				<br>
				{{date('today')}}
				<br> -->
				{{ \Nopaad\Persian::correct( $orders->where('updated_at','>', date('yesterday'))->count() ) }}  سفارش -
				{{ \Nopaad\Persian::correct( number_format( $orders->where('updated_at','>',date('yesterday'))->sum('price') , 0, '',',') ) }} تومان
				</div><div class="col-sm-6">
				<h3 class="page-header">فروش هفته</h3> 
				{{ \Nopaad\Persian::correct( $orders->where('updated_at','>',date('-1 week'))->count() ) }}  سفارش -
				{{ \Nopaad\Persian::correct( number_format( $orders->where('updated_at','>',date('-1 week'))->sum('price') , 0, '',',') ) }} تومان
				</div><div class="col-sm-6">
				<h3 class="page-header" >فروش ماه</h3> 
				{{ \Nopaad\Persian::correct( $orders->where('updated_at','>',date('-1 mounth'))->count() ) }}  سفارش -
				{{  \Nopaad\Persian::correct( number_format( $orders->where('updated_at','>',date('-1 mounth'))->sum('price') , 0, '',',') ) }} تومان
				</div><div class="col-sm-6">
				<h3 class="page-header" >فروش کل</h3> 
				{{ \Nopaad\Persian::correct( $orders->count() ) }}  سفارش -
				{{  \Nopaad\Persian::correct( number_format( $orders->sum('price')  , 0, '',',') ) }} تومان
				</div><div class="col-sm-6">
				<h3 class="page-header">میزان مبلغ طلبکاری</h3> 
				{{ \Nopaad\Persian::correct( $orders->where('updated_at','<',date('today'))->count() ) }}  سفارش -
				{{  \Nopaad\Persian::correct( number_format( $orders->where('updated_at','<',date('today'))->sum('price') , 0, '',',') ) }} تومان
				</div>
			</div>
			@can('manager')
			<div class="seperate"></div>
			<div class="seperate"></div>
			<div class="seperate"></div>
			<div class="row">
				<div class="col-xs-12">
					<h4 class="page-header">فرم ورود اطلاعات واریز های به رستوران‌ها:</h4>
					<div class="seperate"></div>
					<form class="form-inline" method="post" action="/admin/report/variz">
					  	<div class="form-group">
					    	<label for="Invoice_number">شماره پیگیری:</label>
					    	<input type="text" class="form-control" id="Invoice_number" name="Invoice_number" required>
					  	</div>
					  	<div class="form-group">
					    	<label for="price">مبلغ: (تومان)</label>
					    	<input type="number" class="form-control" id="price" name="price" required>
					  	</div>
					  	<div class="form-group">
						  	<label for="restaurant">رستوران:</label>
						  	<select class="form-control" id="restaurant" name="restaurant_id" required>
						  		<option value="">رستوران را انتخاب کنید</option>
						  		@foreach(\App\Models\Product::get() as $restaurant)
							    <option value="{{ $restaurant->id }}">
								    {{ $restaurant->name }} - ({{ $restaurant->city }})
							    </option>
							    @endforeach
						  	</select>
						</div>
					  	<button type="submit" class="btn btn-default">ذخیره</button>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h3 class="page-header"> لیست واریز ها </h3>
					<div class="table-responsive ">
					<table class="table table-hover table-striped">
						<tr>
							<th width="40">شناسه</th>
							<th>مبلغ</th>
							<th>رستوران</th>
							<th>شماره پیگیری</th>
							<th>تلفن کاربر</th>
							<th>تاریخ</th>
						</tr>

					</table>
					</div>
				</div>
			</div>
			@endcan
		</div>
		</div>
	</div>
</div>
@endsection