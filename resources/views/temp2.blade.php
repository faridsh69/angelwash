
<div class="row">
    <div class="col-xs-12 text-center">
    <h3>مربیان</h3>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <ul class="ch-grid">
            <li>
                <div class="ch-item ch-img-1">
                    <div class="ch-info">
                        <h3>Brainiac</h3>
                        <p>by Daniel Nyari <a href="http://drbl.in/eODP">View on Dribbble</a></p>
                    </div>
                </div>
            </li>
            <li>
                <div class="ch-item ch-img-2">
                    <div class="ch-info">
                        <h3>Vision</h3>
                        <p>by Daniel Nyari <a href="http://drbl.in/eNXW">View on Dribbble</a></p>
                    </div>
                </div>
            </li>
            <li>
                <div class="ch-item ch-img-3">
                    <div class="ch-info">
                        <h3>Cylon</h3>
                        <p>by Daniel Nyari <a href="http://drbl.in/eNXY">View on Dribbble</a></p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>


<div class="row" style="display: none;">
    <div class="col-xs-12 text-center">
        <h3>
            ثبت‌نام سریع
        </h3>
    </div>
</div>
<div class="row" style="display: none;">
    <div class="col-xs-12 ltr">
        <form id="theForm" class="simform text-right" autocomplete="off">
            <div class="simform-inner">
                <ol class="questions">
                    <li>
                        <span><label for="q1">نام خود را وارد کنید؟</label></span>
                        <input id="q1" name="q1" type="text"/>
                    </li>
                    <li>
                        <span><label for="q2">نام خانوادگی؟</label></span>
                        <input id="q2" name="q2" type="text"/>
                    </li>
                    <li>
                        <span><label for="q3">ایمیلت ؟</label></span>
                        <input id="q3" name="q3" type="text"/>
                    </li>
                    <li>
                        <span><label for="q4">تلفنت ؟‌شاید لازم شد زنگت بزنیم</label></span>
                        <input id="q4" name="q4" type="text"/>
                    </li>
                    <li>
                        <span><label for="q5">روش آشناییت با ما</label></span>
                        <input id="q5" name="q5" type="text"/>
                    </li>
                    <li>
                        <span><label for="q6">رمز عبور؟</label></span>
                        <input id="q6" name="q6" type="text"/>
                    </li>
                </ol>
                <button class="submit" type="submit">ارسال جواب‌ها</button>
                <div class="controls">
                    <button class="next"></button>
                    <div class="progress"></div>
                    <span class="number">
                        <span class="number-current"></span>
                        <span class="number-total"></span>
                    </span>
                    <span class="error-message"></span>
                </div>
            </div>
            <span class="final-message"></span>
        </form>         
    </div>
</div>


<div class="row text-center" style="background-color: white">
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-xs-12 ">
        <h2 class="bold">
            <!-- <span class="glyphicon glyphicon-copy"></span> -->
            راه‌های ورود به مرکز کارآفرینی طرفه نگار
        </h2>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="col-sm-4">
        <img src="/public/img/why-3.png" width="70px" alt="مرحله ۱" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            شتابدهی
        <div class="seperate"></div>
        <small>این راه ورود مناسب افرادی است که تنها ایده دارند و برای رسیدن به محصول نیاز به کمک زیادی از ما دارند.</small>
        </h4>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-2.png" width="70px" alt="مرحله ۲" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            سرمایه‌گذاری
        <div class="half-seperate"></div>
        <small>این پلن برای کسانی است که محصول اولیه یا نهایی دارند و برای رشد آن نیاز به سرمایه گذار دارند</small>
        </h4>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-1.png" width="60px" alt="مرحله ۳" class="bighover">
        <div class="half-seperate"></div>
        <h4 class="bold">
            همکاری
            <div class="half-seperate"></div>
        <small>افرادی که محصول و کاربر دارند و برای رشد قسمتی از آن نیاز به همکاری با طرفه‌نگار دارند.</small>
        </h4>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="main">
        <div class="fs-slider" id="fs-slider">
            <figure>
                <img src="/public/img/a41.png" alt="image01" />
                <figcaption>
                    <h3>Eloquence</h3>
                    <p>American apparel flexitarian put a bird on it, mixtape typewriter irony aesthetic. </p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a42.png" alt="image02" />
                <figcaption>
                    <h3>Quintessential</h3>
                    <p>Cardigan craft beer mixtape, skateboard forage fixie truffaut messenger bag. </p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a43.png" alt="image03" />
                <figcaption>
                    <h3>Surreptitious</h3>
                    <p>Truffaut iphone flexitarian, banh mi thundercats cliche wolf biodiesel gastropub. </p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a44.png" alt="image04" />
                <figcaption>
                    <h3>Imbroglio</h3>
                    <p>Scenester fixie pickled, shoreditch fugiat enim craft beer retro mustache.</p>
                </figcaption>
            </figure>
            
            <figure>
                <img src="/public/img/a45.png" alt="image05" />
                <figcaption>
                    <h3>Lissome</h3>
                    <p>Aute sunt aliqua do sapiente. Food truck ut sustainable thundercats four loko.</p>
                </figcaption>
            </figure>
        </div>
    </div>
</div>
